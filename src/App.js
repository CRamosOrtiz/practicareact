import React from 'react';
import './App.css';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import  Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

function App() {
  return (
    <div className="App">  
      <header className="App-header" style={{textAlign: 'center'}}>
        <Typography variant="h2">My blog about my life</Typography>
      </header>
      
      <div className="container" style={{display: 'flex'}}>
      
        <div className="opcion">
        <h2>Menu</h2>
        
        <Button disabled >Today</Button><br/>
        <Button color="primary">Yesterday</Button><br/>
        <Button color="secondary">Last week</Button><br/>
        <Button color="primary">Archive</Button><br/>     
        </div>
        
        <div className="encabezado">
        <Typography variant="h4">Meeting with supervisor</Typography>
        <p>11 Dec 2011
        Today I went to he university bus.
        i had a meth my PhD supervisor.</p>
        
        <Typography variant="h4">NEW CAR!!</Typography>
        <p>12 Dec 2011</p>
        <p>Today I bought my new car. It´s a Honda Acoord and it´s really nice</p>
        <p>I met some friends at a pub</p>
        
        <Typography variant="h4">Visit my parents</Typography>
        <p>10 Dec 20011</p>
        <p>Tried to contact my PhD supervisor. He was out of his office</p>
        <p>I visited my parents and we had a nice dinner together</p>
        </div>
        
        <div className="otro">
        <Typography variant="h5">Last posts</Typography>
        <Button fullWidth color="primary">Meetting with supervisor</Button>
        <Button fullWidth color="secondary">New car!!!</Button>
        <Button fullWidth color="primary">Visit my parents</Button>

        
        </div>
        </div>  
        
        <footer> 
        Contact me <a href="">email@something.com</a>
        </footer>
    </div>
  );
}

export default App;
